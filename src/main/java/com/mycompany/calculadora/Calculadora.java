/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.calculadora;

/**
 *
 * @author extre
 */
public class Calculadora {

    private static int div(int v1, int v2){ 
        return v1/v2;
    }
    
    private static int sum(int v1, int v2){
        return v1+v2;
    }

    private static int mult(int v1, int v2) {
        return v1*v2;
    }

    private static int sub(int v1, int v2) {
        return v1-v2;
    }
    
    public static void main(String[] args) {
        //Métodos de casos de teste
        System.out.println("Divisão de 10/2 = "+div(10,2));
        System.out.println("Soma  de 10+2 = "+sum(10,2));
        System.out.println("Multiplicação de 10*2 = "+mult(10,2));
        System.out.println("Subtração de 10-2 = "+sub(10,2));
        System.out.println("Multiplicação de 9*7 = "+mult(9,7));
        System.out.println("Divisão de 15/3 = "+div(15,3));
        System.out.println("Soma  de 17+22 = "+sum(17,22));
        System.out.println("Subtração de 30-12 = "+sub(30,12));
        System.out.println("Soma  de 50+50 = "+sum(50,50));
        System.out.println("Soma  de 12+24 = "+sum(12,24));
        System.out.println("Multiplicação de 5*5 = "+mult(5,5));
        System.out.println("Subtração de 103-61 = "+sub(103,61));
        System.out.println("Subtração de 98-32 = "+sub(98,32));
        System.out.println("Divisão de 2/2 = "+div(2,2));
        System.out.println("Multiplicação de 11*4 = "+mult(11,4));

    }
}